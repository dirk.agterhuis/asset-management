const fs = require('fs')
const path = require('path')
const git = require('simple-git/promise')

const { version } = require('../package.json')
const file = require('../sfdx-project.json')

;(async () => {
    file.packageDirectories[0].versionName = 'ver ' + version.split('.').slice(0, 2).join('.')
    file.packageDirectories[0].versionNumber = `${version}.NEXT`

    fs.writeFileSync(
        path.join(process.cwd(), '/sfdx-project.json'),
        JSON.stringify(file, null, 2),
        'utf8'
    )
    await git().add('./sfdx-project.json')
})()
