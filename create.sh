if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters - Please pass an org alias for the scratch org"
    exit 1
fi

sfdx org:create:scratch -a $1 -f config/project-scratch-def.json -d -y 30
sfdx texei:package:dependencies:install -r 
sfdx project deploy start -w 200
