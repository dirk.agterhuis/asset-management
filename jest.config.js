const { jestConfig } = require('@salesforce/sfdx-lwc-jest/config')
const setupFilesAfterEnv = jestConfig.setupFilesAfterEnv || []
module.exports = {
    ...jestConfig,
    moduleNameMapper: {
        '^c/legacyComparisonTableStyle$':
            '<rootDir>/src/main/default/code/lwc/legacyComparisonTableStyle/legacyComparisonTableStyle.css',
    },
    setupFiles: ['jest-canvas-mock'],
    setupFilesAfterEnv,
    testTimeout: 10000,
}
