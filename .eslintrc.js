module.exports = {
    env: {
        browser: true,
        node: true,
    },
    parser: '@babel/eslint-parser',
    parserOptions: {
        requireConfigFile: false,
        babelOptions: {
            parserOpts: {
                plugins: ['classProperties', ['decorators', { decoratorsBeforeExport: false }]],
            },
        },
    },
    ignorePatterns: ['**/aura/**/*.js'],
    plugins: ['@lwc/eslint-plugin-lwc', 'import', 'prettier'],
    rules: {
        '@lwc/lwc/no-deprecated': 'error',
        '@lwc/lwc/valid-api': 'error',
        '@lwc/lwc/no-document-query': 'error',
    },
    extends: [
        'eslint:recommended',
        '@salesforce/eslint-config-lwc/recommended',
        '@salesforce/eslint-config-lwc/i18n',
        'plugin:import/warnings',
        'plugin:prettier/recommended',
    ],
}
