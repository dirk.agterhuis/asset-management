# Asset Management

## Intro

Everything related to the management of assets lives in this packages.
- Team: Salesforce
- Domain: Loan Application and Loan Administration domains. 

:bulb: :memo:  For the list of changes on this project, please see the [change log](./CHANGELOG.md).

[[_TOC_]]

## Responsibilities

- Define the salesforce sobject schemas for Assets and related sobjects.
- Creating and updating Assets. 
- Business processes around assets, such as appraisals and renewals. 
- Assigning Assets as 'Collateral' when an Asset is bound to an Agreement.

## API docs

- **Asset**: Interact with the Asset definition in Salesforce

## Getting started

### Create a new scratch org for the project

1. Create a scratch org (Intellij Idea + Illuminated Cloud 2)
    1. Right Click on any folder -> Illuminated Cloud -> Configure Application -> Connection -> DX (Create Scratch Org)
    2. Click `Load Scratch Org Definition File`, select the `project-scratch-def.json` stored in `/config` folder.
    3. Write a custom alias in the `Alias` field. Example: `{projectName}-{developerFirstName}`
        1. All other fields can be customised to your specific needs, or left alone.
    4. Confirm by clicking `Ok`.
2. Select the newly created scratch org
    1. Right Click on any folder -> Illuminated Cloud -> Configure Project -> Connection -> Select the `alias` that was previously created
3. Populate Metadata into the Scratch Org
    1. Authenticate to the DevHub </br> `sfdx force:auth:web:login --setdefaultdevhubusername --setalias HubOrg`
    2. Set the newly created scratch org as your default one (Replace `$SCRATCH_ORG_ALIAS` with the `alias` you have used previously) </br> `sfdx force:config:set defaultusername=$SCRATCH_ORG_ALIAS`
    3. Install the project dependencies to the new scratch org </br> `sfdx texei:package:dependencies:install -r --targetdevhubusername HubOrg -w 1000`
    4. Push current project to the scratch org </br> `sfdx force:source:push`

See: `create.sh` for shortcut

**NOTE**: for non `Intellij Idea + Illuminated Cloud 2` user, please check out the `sfdx` documentation in `Related Docs`

### Development Interactions

#### Deploy metadata/code to the scratch org
To deploy to the scratch org you can use the intellij interface or the command line.
1. Intellij: Right Click on any folder -> Illuminated Cloud -> Push Metadata
2. Command Line: `sfdx force:source:push`
#### Retrieve metadata/code from the scratch org
To retrieve from the scratch org you can use the intellij interface or the command line.
1. Intellij: Right Click on any folder -> Illuminated Cloud -> Pull Metadata
2. Command Line: `sfdx force:source:pull`

### Deploying / releasing

The jobs and the pipeline are defined in our [CI/CD templates](https://gitlab.com/new10/modules/cicd-templates).

Deploying to:
- `dev`: from any branch following our naming standards plus `main`
- `acc`: from `main` branch only
- `prd`: from a release tag (see below)

### Deploying to production
A release tag should be automatically created by a **release bot** on every new commit to `main`. On its respective pipeline you'll find a job with a name similar to `deploy_prd` :rocket:

## FAQ

## Project diagram

## Source code documentation

## Internal dependencies

- commons
- core

## External dependencies

This project does not have any external dependencies, except the inherited ones from the internal dependencies

## Infra resources

- lwc
- apex

## Persistence layer

<details>
    <summary>SObjects</summary>

- **Asset** - representation of an item of property owned by a person or company, regarded as having value and available to meet debts, commitments, or legacies.
- **Asset_Evaluation__c** - evaluation of the eligibility for an asset for an asset the customer would like to acquire

## Entities

**TODO:** link to auto-generated type definitions to ensure this is always up to date with the actual code.

## Folder structure
For folder structure please check out -> [folder structure](https://gitlab.com/new10/salesforce/salesforce-documentation/-/blob/main/coding-standards/apex/folder-structure.md)

## Related docs
- [SFDX CLI](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference_force_org.htm#cli_reference_force_org)
- [Create Scratch Org](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_scratch_orgs_create.htm)

## Decision log